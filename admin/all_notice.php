<?php
  require_once "../config/db.php";
  include "partials/header.php"; 
?>
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">All Notice</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Description</th>
                                        <th>Time</th>
                                        <th>action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                            $query = "SELECT * FROM `notice` ORDER BY `notice_id` DESC";
                            $run = mysqli_query($connect,$query);
                            if(mysqli_num_rows($run) > 0){
                                while($rows = mysqli_fetch_assoc($run)){ ?>
                                    <tr>
                                        <td><?= $rows['notice_id']; ?></td>
                                        <td><?= $rows['subject']; ?></td>
                                        <td><?= $rows['Description']; ?></td>
                                        <td><time class="timeago" datetime="<?= $rows['Date']; ?>"></time></td>
                                        <td><a href="action/add_notice.php?delete_id=<?= $rows['notice_id']; ?>" class="btn btn-danger btn-sm">Delete</a></td>
                                        <!-- <td><?php // echo $dayofweek = date('l', strtotime($rows['Date'])); ?></td> -->
                                    </tr>
                                    <?php }
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'partials/footer.php'; ?>