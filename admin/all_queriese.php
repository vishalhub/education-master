<?php
  require_once "../config/db.php";
  include "partials/header.php"; 
?>
<div class="main-panel">
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">All Notice</h4>
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Subject</th>
                                        <th>Message</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                            $query = "SELECT * FROM `contact` ORDER BY `contact_id` DESC";
                            $run = mysqli_query($connect,$query);
                            if(mysqli_num_rows($run) > 0){
                                while($rows = mysqli_fetch_assoc($run)){ ?>
                                    <tr>
                                        <td><?= $rows['contact_id']; ?></td>
                                        <td><?= $rows['contact_name']; ?></td>
                                        <td><?= $rows['contact_email']; ?></td>
                                        <td><?= $rows['contact_subject']; ?></td>
                                        <td><?= $rows['contact_message']; ?></td>
                                        <td><?php  echo $dayofweek = date('d-m-Y', strtotime($rows['contact_date'])); ?> &nbsp;&nbsp;&nbsp;<time class="timeago" datetime="<?= $rows['contact_date']; ?>"></time></td>
                                        <!-- <td><a href="action/add_notice.php?delete_id=<?= $rows['notice_id']; ?>" class="btn btn-danger btn-sm">Delete</a></td> -->
                                        
                                    </tr>
                                    <?php }
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'partials/footer.php'; ?>